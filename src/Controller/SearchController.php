<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Participant;
use App\Entity\Conversation;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository; 
    }
    /**
     * @Route("/search", name="search")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $donnees = $this->getDoctrine()->getRepository(User::class)->findBy([], ['username'=>'asc']);
        $users = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)
        $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
        6// Nombre de résultats par page
        );
    
        return $this->render('search/index.html.twig', compact('users'));
    }

    
    
}
